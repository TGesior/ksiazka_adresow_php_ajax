<?php include('core/init.php'); ?>

<?php
//Obiekt bazy danych
$db = new Database;


$db->query("SELECT * FROM `contacts`");


$contacts = $db->resultset();
?>
<div class="row">
		<div class="large-12 columns">
			<table>
				<thead>
					<tr>
						<th width="200">Imie</th>
						<th width="130">Telefon</th>
						<th width="200">Email</th>
						<th width="250">Addres</th>
						<th width="100">Grupa</th>
						<th width="150">Opcje</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($contacts as $contact) : ?>
					<tr>
						<td><a href="#"><?php echo $contact->first_name.' '.$contact->last_name; ?></a></td>
						<td><?php echo $contact->phone; ?></td>
						<td><?php echo $contact->email; ?></td>
						<td>
						<ul>
							<li><?php echo $contact->address1; ?></li>
							<li><?php if($contact->address2) echo $contact->address2; ?></li>
							<li><?php echo $contact->city; ?>, <?php echo $contact->state; ?> <?php echo $contact->zipcode; ?></li>
						</ul>
						</td>
						<td><?php echo $contact->contact_group; ?></td>
						<td>
							<ul class="button-group">
								<li>
									<a href="#" class="button tiny" data-reveal-id="editModal<?php echo $contact->id; ?>" data-contact-id="<?php echo $contact->id; ?>">Edyt</a>
									<div id = "editModal<?php echo $contact->id; ?>" data-cid="<?php echo $contact->id; ?>" class="reveal-modal editModal" data-reveal>
										<h2>Edytuj kontakt</h2>
						<form id="editContact" action="#" method="post">
						  <div class="row">
							<div class="large-6 columns">
							  <label>Imie
								<input name="first_name" type="text" placeholder="Wpisz imie" value="<?php echo $contact->first_name; ?>" />
							  </label>
							</div>
							 <div class="large-6 columns">
							  <label>Nazwisko
								<input name="last_name" type="text" placeholder="Wpisz nazwisko" value="<?php echo $contact->last_name; ?>" />
							  </label>
							</div>
						  </div>
						  <div class="row">
							<div class="large-4 columns">
							  <label>Email
								<input name="email" type="email" placeholder="Wpisz adres email" value="<?php echo $contact->email; ?>" />
							  </label>
							</div>
							<div class="large-4 columns">
							  <label>Numer telefonu
								<input name="phone" type="text" placeholder="Wpisz numer telefonu" value="<?php echo $contact->phone; ?>" />
							  </label>
							</div>
							<div class="large-4 columns">
								<label>Grupa
									<select name="contact_group">
										<option value="Rodzina" <?php if($contact->contact_group == 'Family'){echo 'selected';} ?>>Rodzina</option>
										<option value="Znajomi" <?php if($contact->contact_group == 'Friends'){echo 'selected';} ?>>Znajomy</option>
										<option value="Praca" <?php if($contact->contact_group == 'Business'){echo 'selected';} ?>>Praca</option>
									</select>
								</label>
							</div>
						  </div>
						  <div class="row">
							<div class="large-6 columns">
							  <label>Addres 1
								<input name="address1" type="text" placeholder="Wpisz adres" value="<?php echo $contact->address1; ?>" />
							  </label>
							</div>
							 <div class="large-6 columns">
							  <label>Addres 2
								<input name="address2" type="text" placeholder="Wpisz adres 2" value="<?php echo $contact->address2; ?>" />
							  </label>
							</div>
						  </div>
						  <div class="row">
							<div class="large-4 columns">
							  <label>Miasto
								<input name="city" type="text" placeholder="Wpisz miasto" value="<?php echo $contact->city; ?>" />
							  </label>
							</div>
							<div class="large-4 columns">
								<label>Stan USA
								<select name="state">
									<?php foreach($states as $key => $value) : ?>
										<?php 
										if($key == $contact->state){
											$selected = 'selected';
										} else {
											$selected = '';
										}
										?>
										<option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
									<?php endforeach; ?>
								</select>
							  </label>
							</div>
							<div class="large-4 columns">
								<label>Kod pocztowy
								<input name="zipcode" type="text" placeholder="Wpisz kod pocztowy" value="<?php echo $contact->zipcode; ?>" />
							  </label>
							</div>
						  </div>
						  <div class="row">
							<div class="large-12 columns">
							  <label>Notatki
								<textarea name="notes" placeholder="Wpisz notatke"><?php echo $contact->notes; ?></textarea>
							  </label>
							</div>
						  </div>
						  <input type="hidden" name="id" value="<?php echo $contact->id; ?>" />
						  <input name="submit" type="submit" class="add-btn button right small" value="Zatwierdz">
							<a class="close-reveal-modal">&#215;</a>
						</form>
									</div>
								</li>
								<li>
									<form id="deleteContact" action="#" method="post">
										<input type="hidden" name="id" value="<?php echo $contact->id; ?>" />
										<input type="submit" class="delete-btn button tiny secondary alert" value="Usun" />
									</form>
								</li>
							</ul>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>