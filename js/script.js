$(document).ready(function(){
	
	$('#loaderImage').show();
	
	
	showContacts();
	
	
	$(document).on('submit','#addContact', function(){
		
		$('#loaderImage').show();
		
		
		$.post("add_contact.php", $(this).serialize())
			.done(function(data){
				console.log(data);
				$('#addModal').foundation('reveal','close');
				showContacts();
			});
			return false;
	});
	
	
	$(document).on('submit','#editContact', function(){
		
		$('#loaderImage').show();
		
		
		$.post("edit_contact.php", $(this).serialize())
			.done(function(data){
				console.log(data);
				$('.editModal').foundation('reveal','close');
				showContacts();
			});
			return false;
	});
	
	
	$(document).on('submit','#deleteContact', function(){
		
		$('#loaderImage').show();
		
		
		$.post("delete_contact.php", $(this).serialize())
			.done(function(data){
				console.log(data);			
				showContacts();
			});
			return false;
	});
	
});


function showContacts(){
	console.log('Showing Contacts...');
	setTimeout("$('#pageContent').load('contacts.php',function(){$('loaderImage').hide();})",1000);
}


$(document).on('click', '.close-reveal-modal', function() {
	$('.reveal-modal').foundation('reveal', 'close');
});