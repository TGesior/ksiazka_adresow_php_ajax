<?php include('core/init.php'); ?>
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Ajax Address Book| Welcome</title>
    <link rel="stylesheet" href="css/foundation.css" />
	<link rel="stylesheet" href="css/custom.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    <div class="row">
		<div class="large-6 columns">
			<h1>AJAX/PHP - ksiazka kontaktow</h1>
		</div>
		<div class="large-6 columns">
			<a class="add-btn button right small" data-reveal-id="addModal">Dodaj kontakt</a>
			<div id="addModal" class="reveal-modal" data-reveal>
				<h2>Dodaj kontakt</h2>
				<form id="addContact" action="#" method="post">
						  <div class="row">
							<div class="large-6 columns">
							  <label>Imie
								<input name="first_name" type="text" placeholder="Wpisz imie" />
							  </label>
							</div>
							 <div class="large-6 columns">
							  <label>Nazwisko
								<input name="last_name" type="text" placeholder="Wpisz nazwisko" />
							  </label>
							</div>
						  </div>
						  <div class="row">
							<div class="large-4 columns">
							  <label>Email
								<input name="email" type="email" placeholder="Wpisz adres email" />
							  </label>
							</div>
							<div class="large-4 columns">
							  <label>Numer telefonu
								<input name="phone" type="text" placeholder="Wpisz numer telefonu" />
							  </label>
							</div>
							<div class="large-4 columns">
								<label>Grupa
									<select name="contact_group">
										<option value="husker">Rodzina</option>
										<option value="starbuck">Znajomi</option>
										<option value="hotdog">Praca</option>
									</select>
								</label>
							</div>
						  </div>
						  <div class="row">
							<div class="large-6 columns">
							  <label>Addres 
								<input name="address1" type="text" placeholder="Wpisz adres" />
							  </label>
							</div>
							 <div class="large-6 columns">
							  <label>Addres 2
								<input name="address2" type="text" placeholder="Wpisz adres 2" />
							  </label>
							</div>
						  </div>
						  <div class="row">
							<div class="large-4 columns">
							  <label>Miasto
								<input name="city" type="text" placeholder="Wpisz miasto" />
							  </label>
							</div>
							<div class="large-4 columns">
								<label>Stan USA
								<select name="state">
									<option>Wybierz stan</option>
									<?php foreach($states as $key => $value) : ?>
										<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
									<?php endforeach; ?>
								</select>
							  </label>
							</div>
							<div class="large-4 columns">
								<label>Kod pocztowy
								<input name="zipcode" type="text" placeholder="Wpisz kod pocztowy" />
							  </label>
							</div>
						  </div>
						  <div class="row">
							<div class="large-12 columns">
							  <label>Notatki
								<textarea name="notes" placeholder="Wpisz notatki"></textarea>
							  </label>
							</div>
						  </div>
						  <input name="submit" type="submit" class="add-btn button right small" value="Zatwierdz">
							<a class="close-reveal-modal">&#215;</a>
						</form>
			</div>
		</div>
	</div>
    
	<!-- Loading Image -->
	<div id="loaderImage">
		<img src="img/ajax-loader.gif">
	</div>
	
	<!-- Main Content -->
	<div id="pageContent"></div>
	
    <script src="js/vendor/jquery.js"></script>
	<script src="js/script.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
